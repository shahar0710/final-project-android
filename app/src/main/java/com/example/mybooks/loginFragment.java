package com.example.mybooks;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.example.mybooks.databinding.FragmentLoginBinding;
import com.example.mybooks.model.Model;

public class loginFragment extends Fragment {
    FragmentLoginBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater,container,false);
        View view = binding.getRoot();

        binding.loginLoginBtn.setOnClickListener(view1 -> {
            String email = binding.loginEmailEt.getText().toString();
            String password =binding.loginPasswordEt.getText().toString();
             if (email.equals("") || password.equals("")) {
                 Toast.makeText(MyApplication.getMyContext(), "Email and Password are required", Toast.LENGTH_SHORT).show();
                 Log.d("TAG", "login - empty user name or password");
                 return;
             }
             Model.instance().login(email, password, new Model.Listener<String>() {
                 @Override
                 public void onComplete(String data) {
                     if (data == null) {
                         Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_bookListFragment);
                     } else {
                         Toast.makeText(MyApplication.getMyContext(), "Failed to login", Toast.LENGTH_SHORT).show();
                         Log.d("TAG", "failed login " + data);
                     }
                 }
             });
        });

        binding.loginSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_registrationFragment);
            }
        });

        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}