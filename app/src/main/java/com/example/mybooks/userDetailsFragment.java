package com.example.mybooks;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mybooks.databinding.FragmentUserDetailsBinding;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.User;
import com.squareup.picasso.Picasso;

public class userDetailsFragment extends Fragment {

    FragmentUserDetailsBinding binding;
    ActivityResultLauncher<Void> cameraLauncher;
    ActivityResultLauncher<String> galleryLauncher;
    User loggedUser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentActivity parentActivity = getActivity();

        parentActivity.addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menu.removeItem(R.id.userDetailsFragment);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
                return false;
            }
        },this, Lifecycle.State.RESUMED);

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), result -> {
            if (result != null) {
                binding.userDetailsAvatarImv.setImageBitmap(result);
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null){
                binding.userDetailsGalleryIv.setImageURI(result);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentUserDetailsBinding.inflate(inflater,container,false);
        View view = binding.getRoot();

        binding.userLoadingPB.setVisibility(ProgressBar.VISIBLE);
        Model.instance().getUserDetails(new Model.Listener<User>() {
            @Override
            public void onComplete(User data) {
                loggedUser = data;
                if (!loggedUser.getEmail().equals("")) {
                    binding.userDetailsEmailTv.setText(loggedUser.getEmail());
                    binding.userDetailsUsernameTv.setText(loggedUser.getUsername());
                    binding.userDetailsPasswordTv.setText("********");
                    if (!loggedUser.getImageUrl().equals("") &&
                        loggedUser.getImageUrl() != null ) {
                        Picasso.get().load(loggedUser.getImageUrl()).into((ImageView) view.findViewById(R.id.userDetails_avatar_imv));
                    }else{
                        ((ImageView) view.findViewById(R.id.userDetails_avatar_imv)).setImageResource(R.drawable.logo);
                    }
                }
                binding.userLoadingPB.setVisibility(ProgressBar.INVISIBLE);
            }

        });


        binding.userDetailsEditBtn.setOnClickListener(view1 -> {
            ChangeVisibility(true, view1);
        });

        binding.userDetailsCancelBtn.setOnClickListener(view1 -> {
            ChangeVisibility(false, view1);
        });

        binding.userDetailsSaveBtn.setOnClickListener(view1 -> {
            String newPassword = binding.userDetailsPasswordTv.getText().toString();
            // Update password
            if (!newPassword.equals("")) {
                if (newPassword.length() < 8) {
                    Toast.makeText(MyApplication.getMyContext(), "password should be at least 8 characters", Toast.LENGTH_SHORT).show();
                    return;
                }else{
                Model.instance().changePassword(newPassword, new Model.Listener<String>() {
                    @Override
                    public void onComplete(String data) {
                        if (data!= null) {
                            Log.d("TAG", "failed to change password,  " + data);
                        }
                    }
                });}
            }

            //update other details
            binding.userDetailsAvatarImv.setDrawingCacheEnabled(true);
            binding.userDetailsAvatarImv.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) binding.userDetailsAvatarImv.getDrawable()).getBitmap();
            String email = binding.userDetailsEmailTv.getText().toString();
            String username = binding.userDetailsUsernameTv.getText().toString();
            Model.instance().uploadImage(email, bitmap, url -> {
                Model.instance().setUserDetails(email, username, url, new Model.Listener<String>() {
                    @Override
                    public void onComplete(String data) {
                        if (data == null) {
                            Toast.makeText(MyApplication.getMyContext(), "Update details successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            });


            ChangeVisibility(false, view1);
        });

        binding.userDetailsGalleryIv.setOnClickListener(view1 -> galleryLauncher.launch("media/*"));
        binding.userDetailsCameraIv.setOnClickListener(View1 -> cameraLauncher.launch(null));

        return view;
    }
    private void ChangeVisibility(Boolean vis, View view1){
        if(!vis) {
            binding.userDetailsSaveBtn.setVisibility(view1.INVISIBLE);
            binding.userDetailsCancelBtn.setVisibility(view1.INVISIBLE);
            binding.userDetailsEditBtn.setVisibility(view1.VISIBLE);
            binding.userDetailsCameraIv.setVisibility(view1.INVISIBLE);
            binding.userDetailsGalleryIv.setVisibility(view1.INVISIBLE);
            binding.userDetailsUsernameTv.setEnabled(vis);
            binding.userDetailsPasswordTv.setEnabled(vis);
            binding.userDetailsPasswordTv.setText("********");


        }else{
            binding.userDetailsSaveBtn.setVisibility(view1.VISIBLE);
            binding.userDetailsCancelBtn.setVisibility(view1.VISIBLE);
            binding.userDetailsEditBtn.setVisibility(view1.INVISIBLE);
            binding.userDetailsCameraIv.setVisibility(view1.VISIBLE);
            binding.userDetailsGalleryIv.setVisibility(view1.VISIBLE);
            binding.userDetailsUsernameTv.setEnabled(vis);
            binding.userDetailsPasswordTv.setEnabled(vis);
            binding.userDetailsPasswordTv.setText("");
        }
    };

}