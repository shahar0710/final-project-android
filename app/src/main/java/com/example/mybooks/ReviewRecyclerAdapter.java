package com.example.mybooks;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mybooks.databinding.FragmentReviewListRowBinding;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.Review;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReviewRecyclerAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
    ReviewRecyclerAdapter.OnItemClickListener listener;
    public static interface OnItemClickListener{
        void onItemClick(int pos);
    }

    LayoutInflater inflater;
    List<Review> data;
    FragmentReviewListRowBinding binding;
    //TODO: change to better name
    Boolean isFiltered;
    public void setData(List<Review> data){
        this.data = data;
        notifyDataSetChanged();
    }
    public ReviewRecyclerAdapter(LayoutInflater inflater, List<Review> data, Boolean isFiltered){
        this.inflater = inflater;
        this.data = data;
        this.isFiltered = isFiltered;
    }

    void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_review_list_row,parent,false);
        return new ReviewViewHolder(view, listener, data, isFiltered);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewViewHolder holder, int position) {
        Review r = data.get(position);
        holder.bind(r,position);
    }

    @Override
    public int getItemCount() {
        if(data == null)return 0;
        return data.size();
    }
}

class ReviewViewHolder extends RecyclerView.ViewHolder{
    TextView titleTv;
    TextView authorTv;
    RatingBar ratingRb;
    TextView userTv;
    ImageView coverImg;
    List<Review> data;
    ImageView editBt;
    public ReviewViewHolder(@NonNull View itemView, ReviewRecyclerAdapter.OnItemClickListener listener, List<Review> data, Boolean isFiltered) {
        super(itemView);
        this.data = data;
        titleTv = itemView.findViewById(R.id.reviewListRow_name_tv);
        authorTv = itemView.findViewById(R.id.reviewListRow_author_tv);
        ratingRb = itemView.findViewById(R.id.reviewListRow_rating_rb);
        userTv = itemView.findViewById(R.id.reviewListRow_user_tv);
        coverImg = itemView.findViewById(R.id.reviewListRow_cover_iv);
        editBt = itemView.findViewById(R.id.reviewListRow_edit_iv);
        if(isFiltered) {
            itemView.findViewById(R.id.reviewListRow_user_tv).setVisibility(View.GONE);
        } else {
            itemView.findViewById(R.id.reviewListRow_edit_iv).setVisibility(View.GONE);
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = getAdapterPosition();
                String reviewId = data.get(pos).id;
                if(isFiltered) {
                    myReviewsFragmentDirections.ActionMyReviewsFragmentToReviewDetailsFragment action = myReviewsFragmentDirections.actionMyReviewsFragmentToReviewDetailsFragment(reviewId);
                    Navigation.findNavController(view).navigate((action));
                } else {
                    allReviewsFragmentDirections.ActionAllReviewsFragmentToReviewDetailsFragment action = allReviewsFragmentDirections.actionAllReviewsFragmentToReviewDetailsFragment(reviewId);
                    Navigation.findNavController(view).navigate((action));
                }
            }
        });

        editBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = getAdapterPosition();
                String reviewId = data.get(pos).id;
                myReviewsFragmentDirections.ActionMyReviewsFragmentToSetReviewFragment action = myReviewsFragmentDirections.actionMyReviewsFragmentToSetReviewFragment(null, reviewId);
                Navigation.findNavController(view).navigate((action) );
            }
        });
    }

    public void bind(Review r, int pos) {
        titleTv.setText(r.getTitle());
        authorTv.setText(r.getAuthor());
        ratingRb.setRating(r.getRating());
        userTv.setText(r.getUser());
        Picasso.get().load(r.getCoverImg()).into(coverImg);
    }
}


