package com.example.mybooks;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mybooks.databinding.FragmentSetReviewBinding;
import com.example.mybooks.model.Book;
import com.example.mybooks.model.BookModel;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.Review;
import com.squareup.picasso.Picasso;

public class setReviewFragment extends Fragment {
    FragmentSetReviewBinding binding;
    String bookId;
    String reviewId;
    String coverImg;
    String image;
    ActivityResultLauncher<Void> cameraLauncher;
    ActivityResultLauncher<String> galleryLauncher;
    Boolean isImageSelected;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isImageSelected = false;
        FragmentActivity parentActivity = getActivity();

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.setReviewImageIv.setImageBitmap(result);
                    isImageSelected = true;
                }
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null){
                    binding.setReviewImageIv.setImageURI(result);
                    isImageSelected = true;
                }
            }
        });

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bookId = setReviewFragmentArgs.fromBundle(getArguments()).getBookId();
        reviewId = setReviewFragmentArgs.fromBundle(getArguments()).getReviewId();
        binding = FragmentSetReviewBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        if(bookId != null) {
            Book book = BookModel.instance.getBookById(bookId);
            binding.setReviewAuthorTv.getEditText().setText(book.author);
            binding.setReviewTitleTv.getEditText().setText(book.title);
            Picasso.get().load(book.getCoverUrl()).into(binding.setReviewCoverIv);
            coverImg = book.getCoverUrl();
        } else {
            Model.instance().getReviewById(reviewId, (review1)->{
                binding.setReviewAuthorTv.getEditText().setText(review1.author);
                binding.setReviewTitleTv.getEditText().setText(review1.title);
                binding.setReviewRatingRb.setRating(review1.getRating());
                coverImg = review1.coverImg;
                image = review1.getImage();
                binding.setReviewReviewTv.getEditText().setText(review1.getContent());
                Picasso.get().load(review1.getCoverImg()).into(binding.setReviewCoverIv);
                Picasso.get().load(review1.getImage()).into(binding.setReviewImageIv);

            });
        }
        binding.setReviewRatingRb.setIsIndicator(false);
        binding.setReviewReviewTv.setEnabled(true);
        binding.setReviewSaveBt.setText("save");
        binding.setReviewCancelBt.setText("cancel");
        binding.setReviewSaveBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isImageSelected){
                    binding.setReviewImageIv.setDrawingCacheEnabled(true);
                    binding.setReviewImageIv.buildDrawingCache();
                    Bitmap bitmap = ((BitmapDrawable) binding.setReviewImageIv.getDrawable()).getBitmap();
                    Model.instance().uploadImage("review" + binding.setReviewTitleTv.getEditText() + reviewId, bitmap, url->{
                        if (url != null){
                            setReview(view, url);
                            //user.setImageUrl(url);
                        }
                    });
                }else {
                   setReview(view, image);
                }
            }
        });
        binding.setReviewAvatarCameraIv.setOnClickListener(view1->{
            cameraLauncher.launch(null);
        });

        binding.setReviewAvatarGalleryIv.setOnClickListener(view1->{
            galleryLauncher.launch("media/*");
        });
        binding.setReviewCancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).popBackStack();
            }
        });


        return view;
    }

    public void setReview(View view, String url) {
        String title = binding.setReviewTitleTv.getEditText().getText().toString();
        String author = binding.setReviewAuthorTv.getEditText().getText().toString();
        Float rating = binding.setReviewRatingRb.getRating();
        String review = binding.setReviewReviewTv.getEditText().getText().toString();
        String image = url;
        String userId = Model.instance().getUserId();
        Review review1 = new Review(reviewId, title, author, rating, review, userId, image, coverImg, bookId);
        Model.instance().setReview(review1, reviewId, (listener) -> {
            Navigation.findNavController(view).popBackStack();
        });
    }
}