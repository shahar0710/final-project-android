package com.example.mybooks;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.mybooks.model.Model;
import com.example.mybooks.model.Review;

import java.util.List;

public class allReviewsFragmentViewModel extends ViewModel {
    private LiveData<List<Review>> data = Model.instance().getAllReviews();

    LiveData<List<Review>> getData(){
        return data;
    }
}
