package com.example.mybooks;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mybooks.databinding.FragmentAllReviewsBinding;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.Review;

import java.util.LinkedList;
import java.util.List;


public class myReviewsFragment extends Fragment {

    FragmentAllReviewsBinding binding;
    List<Review> data = new LinkedList<>();
    ReviewRecyclerAdapter adapter;
    myReviewsFragmentViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAllReviewsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
//        String userId = Model.instance().getUserId().toString();

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ReviewRecyclerAdapter(getLayoutInflater(),viewModel.getData().getValue(), true);
        binding.recyclerView.setAdapter(adapter);

        Model.instance().EventMyReviewListLoadingState.observe(getViewLifecycleOwner(),status->{
            if(status == Model.LoadingState.LOADING) {
                binding.progressBar.setVisibility(View.VISIBLE);
            } else {
                binding.progressBar.setVisibility(View.GONE);
            }
        });

        viewModel.getData().observe(getViewLifecycleOwner(),list->{
            adapter.setData(list);
        });

        Model.instance().EventMyReviewListLoadingState.observe(getViewLifecycleOwner(),status->{
            //binding.swipeRefresh.setRefreshing(status == Model.LoadingState.LOADING);
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(myReviewsFragmentViewModel.class);
    }

    void reloadData(){
        Model.instance().refreshMyReviews();
    }
}