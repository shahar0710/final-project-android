package com.example.mybooks;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mybooks.model.Book;
import com.example.mybooks.model.Model;
import com.squareup.picasso.Picasso;

import java.util.List;


class BookViewHolder extends RecyclerView.ViewHolder{
    TextView titleTv;
    TextView authorTv;
    TextView subjectTv;
    ImageView coverImg;
    List<Book> data;
    public BookViewHolder(@NonNull View itemView, BookRecyclerAdapter.OnItemClickListener listener, List<Book> data) {
        super(itemView);
        this.data = data;
        titleTv = itemView.findViewById(R.id.booklistRow_Title_TV);
        authorTv = itemView.findViewById(R.id.booklistRow_Author_TV);
        subjectTv = itemView.findViewById(R.id.booklistRow_Subject_TV);
        coverImg = itemView.findViewById(R.id.booklistRow_cover_img);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = getAdapterPosition();
                listener.onItemClick(pos);
            }
        });
    }

    public void bind(Book bk, int pos) {
        titleTv.setText(bk.title);
        authorTv.setText(bk.author);
        subjectTv.setText(bk.subject);
        Picasso.get().load(bk.getCoverUrl()).placeholder(R.drawable.book_image).into(coverImg);
    }
}

public class BookRecyclerAdapter extends RecyclerView.Adapter<BookViewHolder>{
    OnItemClickListener listener;
    public static interface OnItemClickListener{
        void onItemClick(int pos);
    }

    LayoutInflater inflater;
    List<Book> data;
    public void setData(List<Book> data){
        this.data = data;
        notifyDataSetChanged();
        Log.d("TAG", data.get(0).getTitle());
    }
    public BookRecyclerAdapter(LayoutInflater inflater, List<Book> data){
        this.inflater = inflater;
        this.data = data;
    }

    void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       // data = Model.instance().getAllBooks();
        View view = inflater.inflate(R.layout.book_list_row,parent,false);
        return new BookViewHolder(view,listener, data);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book bk = data.get(position);
        holder.bind(bk,position);
    }

    @Override
    public int getItemCount() {
        if(data == null)return 0;
        return data.size();
    }

}

