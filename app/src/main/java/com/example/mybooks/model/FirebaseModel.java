package com.example.mybooks.model;

import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FirebaseModel {
    FirebaseFirestore db;
    FirebaseStorage storage;
    FirebaseAuth auth;

    FirebaseModel() {
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        storage = FirebaseStorage.getInstance();
        auth = FirebaseAuth.getInstance();
    }

    public void getAllReviews(Model.GetAllReviewsListener callback){
        db.collection(Review.COLLECTION_NAME).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Review> reviewList = new LinkedList<>();
                if (task.isSuccessful()){
                    QuerySnapshot jsonsList = task.getResult();
                    for (DocumentSnapshot json: jsonsList){
                        Review rv = Review.fromJson(json.getData());
                        reviewList.add(rv);
                    }
                }
                callback.onComplete(reviewList);
            }
        });
    }

    public void allReviewsSince(Long since, Model.GetAllReviewsListener callback) {
        db.collection(Review.COLLECTION_NAME).
                whereGreaterThanOrEqualTo(Review.LAST_UPDATED, new Timestamp(since,0)).
                get().
                addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Review> reviewList = new LinkedList<>();
                if (task.isSuccessful()){
                    QuerySnapshot jsonsList = task.getResult();
                    for (DocumentSnapshot json: jsonsList){
                        Review rv = Review.fromJson(json.getData());
                        reviewList.add(rv);
                    }
                }
                callback.onComplete(reviewList);
            }
        });
    }

    public void getReviewsById(String id, Model.Listener<Review> callback){
        db.collection(Review.COLLECTION_NAME).document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                Review review = null;
                if (task.isSuccessful()){
                    DocumentSnapshot documentSnapshot = task.getResult();
                    Review rv = Review.fromJson(documentSnapshot.getData());
                    review = rv;
                }
                callback.onComplete(review);
            }
        });
    }

    public void getReviewsByUserId(String userId, Model.Listener<List<Review>> callback){
        db.collection(Review.COLLECTION_NAME).whereEqualTo("user", userId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Review> reviewList = new LinkedList<>();
                if (task.isSuccessful()){
                    QuerySnapshot jsonsList = task.getResult();
                    for (DocumentSnapshot json: jsonsList){
                        Review rv = Review.fromJson(json.getData());
                        reviewList.add(rv);
                    }
                }
                callback.onComplete(reviewList);
            }
        });
    }
    public void myReviewsSince(Long since,String userId, Model.Listener<List<Review>> callback) {
        db.collection(Review.COLLECTION_NAME).
                whereEqualTo("uesr", userId).whereGreaterThanOrEqualTo(Review.LAST_UPDATED, new Timestamp(since,0)).
                get().
                addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        List<Review> reviewList = new LinkedList<>();
                        if (task.isSuccessful()){
                            QuerySnapshot jsonsList = task.getResult();
                            for (DocumentSnapshot json: jsonsList){
                                Review rv = Review.fromJson(json.getData());
                                reviewList.add(rv);
                            }
                        }
                        callback.onComplete(reviewList);
                    }
                });
    }

    public void setReview(Review review, String documentId, Model.Listener<Void> listener) {

        if (documentId.equals("")) {
            FirebaseFirestore rootRef = FirebaseFirestore.getInstance();
            CollectionReference reviewsRef = rootRef.collection(Review.COLLECTION_NAME);
            documentId = reviewsRef.document().getId();
        }
        review.setId(documentId);
        Map<String, Object> jsonReview = review.toJson();
        db.collection(Review.COLLECTION_NAME)
                .document(documentId)
                .set(jsonReview)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        listener.onComplete(null);
                    }
                });
    }

    public String getUserId() {
        return auth.getCurrentUser().getEmail();
    }

    public void getUserDetails(Model.Listener<User> listener) {
        String emailId = getUserId();

        db.collection(User.COLLECTION_NAME)
                .document(emailId)
                .get()
                .addOnCompleteListener(task -> {
                    User user = new User();
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        user = User.fromJSON(document.getData(), document.getId());
                    }
                    listener.onComplete(user);
                });
    }

    void uploadImage(String name, Bitmap bitmap, Model.Listener<String> listener){
        StorageReference storageRef = storage.getReference();
        StorageReference imagesRef = storageRef.child("images/" + name + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                listener.onComplete("failed to upload image");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imagesRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        listener.onComplete(uri.toString());
                    }
                });
            }
        });

    }

    public void setUserDetails(String email, String username, String imageUrl, Model.Listener<String> listener) {
        Map<String, Object> jsonReview = new HashMap<>();
        jsonReview.put("email", email);
        jsonReview.put("username", username);
        jsonReview.put("imageUrl", imageUrl);

        // Sign in success, update UI with the signed-in user's information
        db.collection(User.COLLECTION_NAME)
                .document(email)
                .set(jsonReview)
                .addOnSuccessListener(success -> {
                    listener.onComplete(null);
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        listener.onComplete("failed to set user details");
                    }
                });
    }

    public interface Register {
        void onSuccess();
        void onFailed(String failReason);
    }
    public void register(String email, String password,  Register listener) {
        if (email.equals("") ||
                password.equals("")) {

            listener.onFailed("Email and password are required");
            return;
        }
        if (password.length() < 8) {
            listener.onFailed("The password must be at least 8 characters long");
            return;
        }
        auth.createUserWithEmailAndPassword(email.toString(), password.toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            listener.onSuccess();
                        } else {
                            // If sign in fails, display a message to the user.
                            listener.onFailed("Register failed: " + task.getException());
                        }
                    }
                });
    }

    public void login(String email, String password,  Model.Listener<String> listener) {
        auth.signInWithEmailAndPassword(email.toString(), password.toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success (listener get null)
                            listener.onComplete(null);
                        } else {
                            // If sign in fails, display a message to the user.
                            listener.onComplete("Something broken, try to login again");
                        }
                    }
                });
    }

    public boolean isSignedIn() {
        FirebaseUser currentUser = auth.getCurrentUser();
        return (currentUser != null);
    }

    public void logout() {
        auth.signOut();
    }


    public void changePassword(String password,  Model.Listener<String> listener) {
        auth.getCurrentUser().updatePassword(password).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    listener.onComplete(null);
                } else {
                    listener.onComplete("Update password failed: " + task.getException());
                }
            }
        });
    }
}
