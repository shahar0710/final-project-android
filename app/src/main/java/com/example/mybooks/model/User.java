package com.example.mybooks.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Map;

@Entity
public class User {
    public static final String COLLECTION_NAME = "users";


    @PrimaryKey
    @NonNull
    public String email;
    public String username;
    public String imageUrl;

    public User(){
        this.email = "";
        this.username = "";
        this.imageUrl = "";
    }

    public User(String email, String username, String imageUrl){
        this.email = email;
        this.username = username;
        this.imageUrl = imageUrl;
    }

    public static User fromJSON(Map<String, Object> json, String emailId) {
        String username = (String) json.get("username");
        String imageUrl = (String) json.get("imageUrl");

        User userDetails = new User(emailId, username, imageUrl);
        return userDetails;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
