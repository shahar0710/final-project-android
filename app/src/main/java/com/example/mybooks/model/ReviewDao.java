package com.example.mybooks.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ReviewDao {
    @Query("select * from Review")
    LiveData<List<Review>> getAllReviews();

    @Query("select * from Review where user = :user")
    LiveData<List<Review>> getMyReviews(String user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Review ... reviews);

}
