package com.example.mybooks.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface BookApi {
    @Headers("Content-Type:application/json")
    @GET("/books/?page=1")
    Call<BookGetResult> getBooks();
}
