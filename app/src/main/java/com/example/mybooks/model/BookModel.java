package com.example.mybooks.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookModel {
    final public static BookModel instance = new BookModel();

    final String BASE_URL = "https://gutendex.com";
    Retrofit retrofit;
    BookApi bookApi;
    MutableLiveData<List<Book>> data = new MutableLiveData<>();
    private BookModel(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        bookApi = retrofit.create(BookApi.class);
    }

    public LiveData<List<Book>> getBooks(){
        Call<BookGetResult> call = bookApi.getBooks();
        call.enqueue(new Callback<BookGetResult>() {
            @Override
            public void onResponse(Call<BookGetResult> call, Response<BookGetResult> response) {
                if (response.isSuccessful()){
                    Log.d("tag", response.body().toString());
                    BookGetResult res = response.body();
                    data.setValue(res.getResults());
                }else{
                    Log.d("TAG","get books from API response error");
                }
            }

            @Override
            public void onFailure(Call<BookGetResult> call, Throwable t) {
                Log.d("TAG","get books from api failed, error");
            }
        });
        return data;
    }

    public Book getBookById(String bookId) {
        return data.getValue().stream()
               .filter(book1 -> book1.id.equals(bookId))
               .collect(Collectors.toList()).get(0);
    }
}
