package com.example.mybooks.model;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.core.os.HandlerCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Model {
    private static final Model _instance = new Model();
    private FirebaseModel firebaseModel = new FirebaseModel();
    private Executor executor = Executors.newSingleThreadExecutor();
    private Handler mainHandler = HandlerCompat.createAsync(Looper.getMainLooper());
    final public MutableLiveData<LoadingState> EventReviewListLoadingState = new MutableLiveData<LoadingState>(LoadingState.NOT_LOADING);
    final public MutableLiveData<LoadingState> EventMyReviewListLoadingState = new MutableLiveData<LoadingState>(LoadingState.NOT_LOADING);
    private LiveData<List<Review>> reviewList;
    private LiveData<List<Review>> myReviews;
    AppLocalDbRepository localDb = AppLocalDb.getAppDb();

    private Model(){
    }

    public static Model instance(){
        return _instance;
    }

    List<Book> data = new LinkedList<>();

    public enum LoadingState{
        LOADING,
        NOT_LOADING
    }

    public LiveData<List<Review>> getAllReviews() {
        if(reviewList == null){
            reviewList = localDb.reviewDao().getAllReviews();
            refreshAllReviews();
        }
        return reviewList;
    }

    public LiveData<List<Review>> getMyReviews() {
        String user = getUserId();
        if(myReviews == null){
            myReviews = localDb.reviewDao().getMyReviews(user);
            refreshMyReviews();
        }
        return myReviews;
    }

    public void refreshAllReviews(){
        EventReviewListLoadingState.setValue(LoadingState.LOADING);
        // get local last update
        Long localLastUpdate = Review.getLocalLastUpdate();
        // get all updated recorde from firebase since local last update
        firebaseModel.allReviewsSince(localLastUpdate,list-> {
            executor.execute(()->{
                Long time = localLastUpdate;
                for(Review rv:list) {
                    // insert new records into ROOM
                    localDb.reviewDao().insertAll(rv);
                    if (time < rv.getLastUpdated()) {
                        time = rv.getLastUpdated();
                    }
                }
                // update local last update
                Review.setLocalLastUpdated(time);
                EventReviewListLoadingState.postValue(LoadingState.NOT_LOADING);
            });
        });
    }

    public void refreshMyReviews(){
        String user = getUserId();
        EventMyReviewListLoadingState.setValue(LoadingState.LOADING);
        // get local last update
        Long localLastUpdate = Review.getLocalLastUpdate();
        // get all updated recorde from firebase since local last update
        firebaseModel.myReviewsSince(localLastUpdate,user,list-> {
            executor.execute(()->{
                Long time = localLastUpdate;
                for(Review rv:list) {
                    // insert new records into ROOM
                    localDb.reviewDao().insertAll(rv);
                    if (time < rv.getLastUpdated()) {
                        time = rv.getLastUpdated();
                    }
                }
                // update local last update
                Review.setLocalLastUpdated(time);
                EventMyReviewListLoadingState.postValue(LoadingState.NOT_LOADING);
            });
        });
    }

    public List<Book> getAllBooks(){
        return data;
    }

    public interface GetAllReviewsListener{
        void onComplete(List<Review> data);
    }

    public interface Listener<T>{
        void onComplete(T data);
    }

    public void setReview(Review review, String documentId, Listener<Void> listener){
        firebaseModel.setReview(review,documentId, (Void)->{
            refreshAllReviews();
            listener.onComplete(null);
        });
        //firebaseModel.setReview(review, documentId, listener);
    }

    public String getUserId() {
        return firebaseModel.getUserId();
    }

    public void getReviewById(String reviewId, Listener<Review> callback) {
        firebaseModel.getReviewsById(reviewId, callback);
    }

    public void uploadImage(String name, Bitmap bitmap, Listener<String> listener) {
        firebaseModel.uploadImage(name,bitmap,listener);
    }

    public void setUserDetails(String email, String username, String imageUrl,  Listener<String> listener) {
        firebaseModel.setUserDetails(email, username, imageUrl, listener);
    }

    public void register(String email, String password, FirebaseModel.Register listener) {
        firebaseModel.register(email, password, listener);
    }

    public void login(String email, String password, Listener<String> listener) {
        firebaseModel.login(email, password, listener);
    }

    public boolean isSignedIn() {
        return firebaseModel.isSignedIn();
    }

    public void logout() {
//        reviewList.postValue(null);
//        myReviews.postValue(null);
        firebaseModel.logout();
    }

    public void getUserDetails(Listener<User> listener) {
        firebaseModel.getUserDetails(listener);
    }

    public void changePassword(String password, Listener<String> listener) {
        firebaseModel.changePassword(password, listener);
    }
}
