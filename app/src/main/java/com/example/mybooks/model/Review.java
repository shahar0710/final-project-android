package com.example.mybooks.model;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.mybooks.MyApplication;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;

import java.util.HashMap;
import java.util.Map;

@Entity()
public class Review {
    public static final String COLLECTION_NAME = "Reviews";
    static final String LOCAL_LAST_UPDATED = "reviews_local_last_update";
    static final String LAST_UPDATED = "lastUpdated";
    @PrimaryKey
    @NonNull
    public String id;
    public String title;
    public String author;
    public Float rating;
    public String content;
    @NonNull
    public String user;
    public String image;
    public String coverImg;
    public String bookId;

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public static Long getLocalLastUpdated() {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        return sharedPref.getLong(LOCAL_LAST_UPDATED, 0);

    }

    public static void setLocalLastUpdated(Long lastUpdated) {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LOCAL_LAST_UPDATED, lastUpdated);
        editor.commit();

    }

    public Long lastUpdated;

    public Review(String id, String title, String author, Float rating, String content, String user,
    String image, String coverImg, String bookId) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.rating = rating;
        this.content = content;
        this.user = user;
        this.image = image;
        this.coverImg = coverImg;
        this.bookId = bookId;
    }

    public static Long getLocalLastUpdate() {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        return sharedPref.getLong(LOCAL_LAST_UPDATED, 0);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImages(String image) {
        this.image = image;
    }
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public static Review fromJson(Map<String,Object> json) {
        String id = (String)json.get("id");
        String title = (String)json.get("title");
        String author = (String)json.get("author");
        Float rating = ((Double)json.get("rating")).floatValue();
        String content = (String)json.get("content");
        String user = (String)json.get("user");
        String image = (String)json.get("image");
        String coverImg = (String)json.get("coverImg");
        String bookId = (String)json.get("bookId");
        Review rv = new Review(id,title, author,rating,content,user,image,coverImg,bookId );
        try{
            Timestamp time = (Timestamp) json.get(LAST_UPDATED);
            rv.setLastUpdated(time.getSeconds());
        }catch(Exception e){

        }
        return rv;
    }

    public Map<String, Object> toJson() {
        Map<String, Object> json = new HashMap<>();
        json.put("id", getId());
        json.put("title", getTitle());
        json.put("author", getAuthor());
        json.put("rating", getRating());
        json.put("content", getContent());
        json.put("user", getUser());
        json.put("image", getImage());
        json.put("coverImg", getCoverImg());
        json.put("bookId", getBookId());
        json.put(LAST_UPDATED, FieldValue.serverTimestamp());
        return json;
    }

}
