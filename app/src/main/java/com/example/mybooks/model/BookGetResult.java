package com.example.mybooks.model;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BookGetResult {
    public List<BookFromApi> results;

    public List<Book> getResults() {
        List<Book> booksFromApi = new LinkedList<>();
        for(BookFromApi bk: results) {
            Integer subjectSize = bk.subjects.size() -1;
                Book newBook = new Book(bk.id,bk.title, bk.authors.get(0).get("name").toString(), bk.subjects.get(subjectSize).toString(),
                        bk.formats.get("image/jpeg").toString());
                newBook.setCoverUrl(newBook.getCoverUrl().substring(1,newBook.getCoverUrl().length() - 1));
                newBook.setAuthor(newBook.getAuthor().substring(1,newBook.getAuthor().length() - 1));
                booksFromApi.add(newBook);
        }
        return booksFromApi;
    }

    public class BookFromApi {
        public String id;
        public String title;
        public List<JsonObject> authors;
        public List<String> subjects;
        public List<JsonObject> translators;
        public List<String> bookshelves;
        public List<String> languages;
        public Boolean copyright;
        public String media_type;
        public JsonObject formats;
        public Integer download_count;
    }
}
