package com.example.mybooks;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;

import com.example.mybooks.databinding.FragmentRegistrationBinding;
import com.example.mybooks.model.FirebaseModel;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.User;



public class registrationFragment extends Fragment {

    FragmentRegistrationBinding binding;
    ActivityResultLauncher<Void> cameraLauncher;
    ActivityResultLauncher<String> galleryLauncher;
    Boolean isAvatarSelected = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentActivity parentActivity = getActivity();
        setHasOptionsMenu(true);

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), result -> {
            if (result != null) {
                binding.registerAvatarIv.setImageBitmap(result);
                isAvatarSelected = true;
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null){
                binding.registerAvatarIv.setImageURI(result);
                isAvatarSelected = true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRegistrationBinding.inflate(inflater,container,false);
        View view = binding.getRoot();

        binding.registerRegisterBtn.setOnClickListener(view1 ->{
            String email = binding.registerEmailEt.getText().toString();
            String username = binding.registerUsernameEt.getText().toString();
            String password = binding.registerPasswordEt.getText().toString();
            User user = new User(email,username,"");

            if (email.equals("") || password.equals("") || username.equals("")) {
                Toast.makeText(MyApplication.getMyContext(), "Email, Password and Username are required", Toast.LENGTH_SHORT).show();
                return;
            }
            if (password.length() < 8) {
                Toast.makeText(MyApplication.getMyContext(), "The password must be at least 8 characters long", Toast.LENGTH_SHORT).show();
                return;
            }


            Model.instance().register(email, password, new FirebaseModel.Register() {
                @Override
                public void onSuccess() {
                    if (isAvatarSelected) {
                        binding.registerAvatarIv.setDrawingCacheEnabled(true);
                        binding.registerAvatarIv.buildDrawingCache();
                        Bitmap bitmap = ((BitmapDrawable) binding.registerAvatarIv.getDrawable()).getBitmap();
                        Model.instance().uploadImage(email, bitmap, url -> {
                            if (url != null) {
                                user.setImageUrl(url);
                            }
                        });
                    }
                    Model.instance().setUserDetails(user.email, user.username, user.imageUrl, (unused) -> Navigation.findNavController(getView()).navigate(R.id.action_registrationFragment_to_bookListFragment));
                }
                @Override
                public void onFailed(String err) {
                    Toast.makeText(MyApplication.getMyContext(), "Failed to register, try again later :(", Toast.LENGTH_SHORT).show();
                    Log.d("TAG", "faild to register in firebase- " + err);
                }
            });

        });

        binding.registerAvatarCameraIv.setOnClickListener(view1-> cameraLauncher.launch(null));

        binding.registerAvatarGalleryIv.setOnClickListener(view1-> galleryLauncher.launch("media/*"));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}