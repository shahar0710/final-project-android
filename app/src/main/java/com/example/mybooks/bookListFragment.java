package com.example.mybooks;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mybooks.databinding.FragmentBookListBinding;
import com.example.mybooks.model.Book;
import com.example.mybooks.model.BookModel;
import com.example.mybooks.model.Model;

import java.util.LinkedList;
import java.util.List;

public class bookListFragment extends Fragment {
    FragmentBookListBinding binding;
    List<Book> data = new LinkedList<>();
    BookRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentBookListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BookRecyclerAdapter(getLayoutInflater(),data);
        binding.recyclerView.setAdapter(adapter);
        LiveData<List<Book>> books = BookModel.instance.getBooks();
        binding.booklisrProgressBar.setVisibility(View.VISIBLE);
        books.observe(getViewLifecycleOwner(),list->{
            adapter.setData(list);
            binding.booklisrProgressBar.setVisibility(View.GONE);
        });

        adapter.setOnItemClickListener(new BookRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Book bk = books.getValue().get(pos);
                bookListFragmentDirections.ActionBookListFragmentToAddReviewFragment action = bookListFragmentDirections.actionBookListFragmentToAddReviewFragment(bk.id, "");
                Navigation.findNavController(view).navigate((NavDirections) action);
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        return view;
    }
}