package com.example.mybooks;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mybooks.databinding.FragmentAllReviewsBinding;;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.Review;

import java.util.LinkedList;
import java.util.List;


public class allReviewsFragment extends Fragment {

    FragmentAllReviewsBinding binding;
    List<Review> data = new LinkedList<>();
    allReviewsFragmentViewModel viewModel;
    ReviewRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAllReviewsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ReviewRecyclerAdapter(getLayoutInflater(),viewModel.getData().getValue(), false);
        binding.recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ReviewRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Log.d("TAG", "Row was clicked " + pos);
                Review rv = viewModel.getData().getValue().get(pos);
                allReviewsFragmentDirections.ActionAllReviewsFragmentToReviewDetailsFragment action = allReviewsFragmentDirections.actionAllReviewsFragmentToReviewDetailsFragment(rv.id);
                Navigation.findNavController(view).navigate(action);
            }
        });

        Model.instance().EventReviewListLoadingState.observe(getViewLifecycleOwner(),status->{
            if(status == Model.LoadingState.LOADING) {
                binding.progressBar.setVisibility(View.VISIBLE);
            } else {
                binding.progressBar.setVisibility(View.GONE);
            }
        });

        viewModel.getData().observe(getViewLifecycleOwner(),list->{
            adapter.setData(list);
        });

        Model.instance().EventReviewListLoadingState.observe(getViewLifecycleOwner(),status->{
            //binding.swipeRefresh.setRefreshing(status == Model.LoadingState.LOADING);
        });

//        binding.swipeRefresh.setOnRefreshListener(()->{
//            reloadData();
//        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(allReviewsFragmentViewModel.class);
    }


    void reloadData(){
        Model.instance().refreshAllReviews();
    }

}