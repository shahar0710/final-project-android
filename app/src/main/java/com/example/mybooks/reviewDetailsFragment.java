package com.example.mybooks;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mybooks.databinding.FragmentReviewDetailsBinding;
import com.example.mybooks.model.Model;
import com.example.mybooks.model.Review;
import com.squareup.picasso.Picasso;

public class reviewDetailsFragment extends Fragment {
    FragmentReviewDetailsBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String documentId = reviewDetailsFragmentArgs.fromBundle(getArguments()).getReviewId();
        Model.instance().getReviewById(documentId, (review1)->{
            setReview(review1);

        });
        binding = FragmentReviewDetailsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        return view;
    }

    public void setReview(Review review) {
        binding.reviewDetailsTitleTv.setText(review.getTitle());
        binding.reviewDetailsUserTv.setText(review.getUser());
        binding.reviewDetailsAuthorTv.setText(review.getAuthor());
        binding.reviewDetailsReviewTv.setText(review.getContent());
        binding.reviewDetailsRatingRb.setRating(review.getRating());
        if(review.getImage() != ""){
            Picasso.get().load(review.getImage()).into(binding.reviewDetailsImagesIv);
        }
        Picasso.get().load(review.getCoverImg()).into(binding.reviewDetailsCoverIv);
    }
}